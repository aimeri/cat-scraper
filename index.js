require('dotenv').config()
const puppeteer = require('puppeteer')
const mongoose = require('mongoose')
const nodemailer = require('nodemailer')
const express = require("express")
const app = express()
const cors = require('cors')
const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
});

mongoURI = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`
let catSchema = new mongoose.Schema({
    Title: String,
    Description: String,
    Price: String,
    Location: String,
    Pictures: [String],
    Link: String,
    Posted: String,
    Emailed: {
        type: Number,
        default: 0
    }
});

let Cat = mongoose.model('cat-scraping', catSchema);

app.use(cors())


app.get("/", (req, res) => {
    scrape()
    .then(
        () => {
            Cat.find(function (err, docs) {
                if (err) return console.error(err);
                docs.map(doc => {
                    if (doc.Emailed === 0) {
                        emailCat(doc)
                        doc.Emailed = 1
                        upsertCat(doc)
                    }
                })
            })
        }
    )
    .then(
        res.end("Scraping finished")
    )
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, function () {
    console.log(`App listening on port ${PORT}`);
});



//EMAIL SETUP  
function emailCat({ Title, Description, Price, Location, Pictures, Posted, Link }) {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASSWORD
        }
    });

    var mailOptions = {
        from: 'aimeri@baddouh.me',
        to: 'aimeri@baddouh.me',
        subject: `*** CAT ALERT *** - ${Title} - ${Price}`,
        html: `<html style="">
        <body style="font-family: verdana; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; background-color: rgba(68,0,153,0.5);">
            <div id="root" style="padding: 20px;">
                <div align="center" style="font-size: 30px; font-weight: bold; margin: 0 auto; width: 500px; border-radius: 10px; background-color: white;">
                    <p style="color: rgba(68,0,153,0.5);">My Future Kitty</p>
                </div>
                <div class="wrapper" style="border-radius: 10px; margin: 10px auto; width: 500px;  overflow: hidden;">
                    <div class="card" style="background-color: white; border-radius: 10px;">
                        <img src="https://res.cloudinary.com/slothcrew/image/upload/q_30/v1542562383/${Pictures[0]}.jpg" alt="" style=" width: 500px;">
                        <h1 style="color: #a1a1a1; font-size: 13px; margin: 20px 0px 0px 20px" class="title">${Title}</h1>
                        <p style="margin: 20px 20px 0px 20px" class="description">${Description}</p>
                        <p style="margin: 20px 0px 0px 20px; display: inline-block;" class="price">Price: ${Price}</p>
                        <p style="margin: 20px 20px 0px 20px; display: inline-block;" class="location">Location: ${Location}</p>
                        <p style="margin: 20px 0px 0px 20px" class="post-link">Hoobly Link: <a style="color: rgba(68,0,153,0.5)" href="${Link}">${Link}</a></p>
                        <ul style="padding: 0px; color: white;" align="center">
                            ${Pictures.map(picture => `<li style="display: inline; overflow: hidden; background-position: center center; background-repeat: no-repeat; margin: 0px 0px 10px 0px; color: white;"><a href="${Link}"><img src="https://res.cloudinary.com/slothcrew/image/upload/c_fill,g_face,h_100,q_auto:low,r_max,w_100/v1542562383/${picture}.jpg" alt="" width=100px height=100px style="border-radius: 50%; color: white;"></a></li>`)}
                        </ul>
                        <p align="right" style="color: #a1a1a1; margin: 20px 20px 10px 20px;" class="posted">Date Posted: ${Posted}</p>
                    </div>
                    <p align="center" style="color: #f1f1f1;">Copyright Slothcrew 2018</p>
                </div>
            </div>
            
        </body>
        </html>
      `
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) { console.log(error) }
    });
}

//Scraper setup
const scrape = async () => {
    if (mongoose.connection.readyState == 0) {
        const config = { useNewUrlParser: true, useFindAndModify: false };
        mongoose.connect(mongoURI, config);
    }
    const browser = await puppeteer.launch({ args: ['--no-sandbox'] })
    const page = await browser.newPage()
    let pagination = 0
    //sphynx cats in NC
    const baseURL = 'http://www.hoobly.com/12019/1950/'
    let firstPage = baseURL + pagination
    let arrayOfLinks = []
    let crawlPages = async url => {
        await page.goto(url)
        let links = await page.$$eval('body > div > div > div.col-sm-9.col-md-9.col-lg-9 > table > tbody > tr > td:nth-child(2) > h4 > a',
            mylink => mylink.map(link => link.href))

        if (links[links.length - 1] === arrayOfLinks[arrayOfLinks.length - 1]) {
            return
        }
        else {
            arrayOfLinks.push(...links)
            pagination = pagination + 10
            let nextPage = baseURL + pagination
            await crawlPages(nextPage)

        }
    }
    await crawlPages(firstPage)



    for (link of arrayOfLinks) {
        let catLinksFromDB = await Cat.findOne({ Link: link })
        let catLinks
        if (catLinksFromDB !== null) {
            catLinks = catLinksFromDB.Link
        } else {
            catLinks = 'null'
        }
        if (link !== catLinks) {
            await page.goto(link)
            let title = await page.$eval('.panel-title', title => title.innerText)
            let description = await page.$eval('#listing > table > tbody > tr > td > p',
                description => description.innerText.trim().replace(/\n/g, ' '))
            let price = await page.$eval('.panel-info > table > tbody > tr:nth-child(1) > td:nth-child(2)',
                price => price.innerText)
            let location = await page.$eval('.panel-info > table > tbody > tr:nth-child(2) > td:nth-child(2)',
                location => location.innerText)
            let images = await page.$$eval('#images > a',
                images => Array.from(images).map(image => image.href))
            let posted = await page.$eval('.panel-info > table > tbody > tr:nth-child(4) > td:nth-child(2)',
                posted => posted.innerText)
 
            let multipleUpload = async () => {
                let upload_res = new Array();

                for (let picture of images) {
                    await cloudinary.v2.uploader.upload(picture,{ folder: 'cat-scraper'}, (error, result) => {

                        if (result) {
                            /*push public_ids in an array */
                            upload_res.push(result.public_id);
                        } else if (error) {
                            console.log(error)
                            reject(error)
                        }

                    })

                }
                return upload_res
            }

            /* Waits for pictures to be uploaded and have result array with public_ids */
            let pictures = await multipleUpload()

            upsertCat({
                Title: title,
                Description: description,
                Price: price,
                Location: location,
                Link: link,
                Pictures: pictures,
                Posted: posted
            });
        }

    }
    await browser.close();
}

//DB Preparation
function upsertCat(catObj) {
    if (mongoose.connection.readyState == 0) {
        const config = { useNewUrlParser: true, useFindAndModify: false };
        mongoose.connect(mongoURI, config);
    }

    // if this email exists, update the entry, don't insert
    const conditions = { Title: catObj.Title };
    const options = { keepAlive: 300000, connectTimeoutMS: 30000, upsert: true, new: true, setDefaultsOnInsert: true };

    Cat.findOneAndUpdate(conditions, catObj, options, (err, result) => {
        if (err) throw err;
    });
}
/*
setInterval(function () {
    scrape().then(
        () => {
            Cat.find(function (err, docs) {
                if (err) return console.error(err);
                docs.map(doc => {
                    if (doc.Emailed === 0) {
                        emailCat(doc)
                        doc.Emailed = 1
                        upsertCat(doc)
                    }
                })
            })
        }
    )
}, 60000)


setInterval(function() {
    https.get("https://cat-scraper.herokuapp.com");
}, 300000); // every 5 minutes (300000)
*/